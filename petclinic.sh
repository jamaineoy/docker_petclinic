#!/bin/bash

echo "DBSERVERNAME=$DBSERVERNAME"
echo "DBUSERNAME=$DBUSERNAME"
echo "DBPASSWORD=$DBPASSWORD"

cd /app
mkdir config
mysql -h $DBSERVERNAME -u $DBUSERNAME --password="$DBPASSWORD" < init_db.sql
sed -e "s/DBSERVERNAME/$DBSERVERNAME/" \
    -e "s/DBUSERNAME/$DBUSERNAME/" \
    -e "s/DBPASSWORD/$DBPASSWORD/" application.properties.tmplt > config/application.properties

cat config/application.properties

java -jar ./petclinic.jar